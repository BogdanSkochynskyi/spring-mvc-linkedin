package com.test.hplus.controllers;

import com.test.hplus.beans.Product;
import com.test.hplus.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.async.DeferredResult;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@Controller
public class SearchController {

    @Autowired
    private ProductRepository productRepository;

    @Qualifier("mvcTaskExecutor")
    @Autowired
    private AsyncTaskExecutor executor;

    /** First variant of async request processing*/
//    @GetMapping("/search")
//    public Callable<String> search(@RequestParam("search") String search, Model model, HttpServletRequest request) {
//        System.out.println("In search controller");
//        System.out.println("Search criteria: " + search);
//        System.out.println("Asynk supported in app " + request.isAsyncSupported());
//        System.out.println("Thread from the servlet controller " + Thread.currentThread().getName());
//
//        return () -> {
//            Thread.sleep(3000); //To simulate delays
//            System.out.println("Thread from the spring mvc task executor " + Thread.currentThread().getName());
//            List<Product> products = new ArrayList<>();
//            products = productRepository.searchByName(search);
//            model.addAttribute("products", products);
//            return "search";
//        };
//    }

    /** Second variant of async request processing*/
    @GetMapping("/search")
    public DeferredResult<String> search(@RequestParam("search") String search, Model model, HttpServletRequest request) {
        DeferredResult<String> deferredResult = new DeferredResult<>();

        System.out.println("In search controller");
        System.out.println("Search criteria: " + search);
        System.out.println("Asynk supported in app " + request.isAsyncSupported());
        System.out.println("Thread from the servlet controller " + Thread.currentThread().getName());

        executor.execute(() -> {
            try {
                Thread.sleep(3000); //To simulate delays
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread from the spring mvc task executor " + Thread.currentThread().getName());
            List<Product> products = new ArrayList<>();
            products = productRepository.searchByName(search);
            model.addAttribute("products", products);
            deferredResult.setResult("search");
        });

        return deferredResult;
    }

}
